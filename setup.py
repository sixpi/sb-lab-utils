from setuptools import setup, find_packages


with open("requirements.txt", "r") as requirements_file:
    requirements = [x.strip() for x in requirements_file.readlines()]

setup(
    name="sblu",
    version="0.1",
    packages=find_packages(),

    author="Bing Xia",
    author_email="sixpi@bu.edu",

    install_requires=requirements
)
