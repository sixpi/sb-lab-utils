class memoize(dict):
    """
    Basic memoizing class, taken from http://wiki.python.org/moin/PythonDecoratorLibrary#Alternate_memoize_as_dict_subclass
    """
    def __init__(self, func):
        self.func = func
        self.memoized = {}

    def __call__(self, *args):
        return self[args]

    def __missing__(self, key):
        result = self[key] = self.func(*key)
        return result
