sb-lab
======

Python library to access the servers of the Structural Bioinformatics
Lab and deal with the files in a programmatic way.

Requires you can log to the server using SSH using an SSH key you have
set up beforehand.


Installation
------------

Clone the repository to your local machine first:

    git clone git@bitbucket.org:sixpi/sb-lab-utils.git

After cloning, you should be able to install using:

    cd sb-lab-utils
    [sudo] python setup.py install

`sudo` is only needed if you are installing it globally, and not in a
virtualenv(http://www.virtualenv.org/).


Usage
-----

First, copy the include config.example to `$HOME/.sblu/config`, and
set the values inside to your credentials (your username, your
password, etc.). For security, I would recommend setting the
permissions on the entire `.sblu` directory to read and write only by
you (`chmod 700 $HOME/.sblu`). This renders everything in the
directory safe from others (only really important if you store your
password in the file).

Some basic examples are included in the examples directory.
