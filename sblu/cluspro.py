from contextlib import contextmanager
import socket
import paramiko


CLUSPRO_DATA_DIR = "/data/cluspro"


class ClusproClient(object):
    """Exposes various ways to get at files and directories on cluspro"""
    def __init__(self, config):
        self._username = config.get("cluspro", "username")
        self._hostname = config.get("cluspro", "hostname")
        self._port = config.getint("cluspro", "port")

        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sock.connect((self._hostname, self._port))

        self._transport = paramiko.Transport(self._sock)
        try:
            self._transport.start_client()
        except paramiko.SSHException as e:
            print("SSHException:", e.message)
            raise e

        if config.has_option("cluspro", 'check_host_key'):
            pass  # TODO

        agent = paramiko.Agent()
        keys = agent.get_keys()
        for k in keys:
            try:
                self._transport.auth_publickey(self._username, k)
            except paramiko.SSHException:
                pass
            if self._transport.is_authenticated():
                break

        if not self._transport.is_authenticated():
            raise Exception("No public keys worked")

        self._sftp = paramiko.SFTPClient.from_transport(self._transport)

    def __enter__(self):
        return self

    def __exit__(self, type, value, tb):
        self.close()

    def close(self):
        self._sftp.close()
        self._transport.close()
        self._sock.close()

    @contextmanager
    def ftfile(self, cluspro_id, coeff_set, translation=0, mode="r"):
        ftfile_format = "ft.{0:03d}.{1:02d}"
        ftfile_filename = ftfile_format.format(coeff_set, translation)

        allfiles = self.listdir(cluspro_id)
        if ftfile_filename in allfiles:
            ftfile = ftfile_filename
            self._sftp.chdir(CLUSPRO_DATA_DIR + "/{0}".format(cluspro_id))
            with self._sftp.open(ftfile, mode) as ftfile:
                yield ftfile
        elif (ftfile_filename + ".gz") in allfiles:
            import gzip

            ftfile = ftfile_filename + ".gz"
            self._sftp.chdir(CLUSPRO_DATA_DIR + "/{0}".format(cluspro_id))
            with self._sftp.open(ftfile, mode) as ftfile:
                with gzip.GzipFile(fileobj=ftfile, mode="r") as gzfile:
                    yield gzfile
        else:
            raise Exception("{0}/ft.{1:03d}.{2:02d}[.gz] not found".format(
                cluspro_id, coeff_set, translation))

    def listdir(self, cluspro_id):
        dirname = "/data/cluspro/{0}".format(cluspro_id)
        return self._sftp.listdir(dirname)

    @contextmanager
    def open(self, cluspro_id, filepath, mode="r"):
        path_format = "/data/cluspro/{0}/{1}"
        remote_path = path_format.format(cluspro_id, filepath)

        with self._sftp.open(remote_path, mode) as f:
            yield f
